# docker-setup

Files for setting up various development environments with Docker. See readme in folder for any specific instructions

Getting Started
====
* [Install Docker](https://www.docker.com/get-started)
* It should provide access to the `docker-compose` command.

Notes
===
* Note that `localhost` does not appear to work when trying to connect from one service to another within the container - you will have to figure out your localhost address or use [`host.docker.internal`](https://stackoverflow.com/questions/24319662/from-inside-of-a-docker-container-how-do-i-connect-to-the-localhost-of-the-mach) instead.
* Keep in mind that this is all intended for __LOCAL__ development. Make sure you make the necessary changes take appropriate precautions when using the files here for production purposes.

Useful Commands
===
- `docker-compose.exe up` - starts up webserver
- `docker-compose.exe exec webserver sh` - gets you shell access into the Docker instance
- `docker-compose.exe up --build` - use this if you make changes to your Docker configuration.

Further Reading
====
- [Good Docker compose example](https://perchrunway.com/blog/2017-01-19-getting-started-with-docker-for-local-development)
