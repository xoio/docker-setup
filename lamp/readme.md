LAMP setup
====
This sets up a fairly typical LAMP stack.


Install
===
* Figure out where your web root will be and make the necessary change in `docker-compose.yml`
* run `docker-compose up`