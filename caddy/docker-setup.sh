#!/bin/bash

# Complete setup for getting Caddy up and running with Docker. 
# 1. Will download and install docker (need root access.)
# 2. Will start to install Caddy, assuming you have a Docker Compose file setup
# 3. Will copy Caddyfile to container 
# 4. Will also attempt to copy a folder named "site" to the container. "site" should hold the contents of your site. 


# install docker - 
# https://docs.docker.com/engine/install/ubuntu/#installation-methods

sudo apt-get update
sudo apt-get install git apt-transport-https ca-certificates curl gnupg lsb-release

# add Docker's GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# install stable release
 echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# now we can install docker engine
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io 

# finally test things
sudo docker run hello-world

# setup docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

# start to set up caddy - first make sure Caddyfile exists. If it does
if test -f "Caddyfile"; then
    # create volume for caddy_data 
    docker volume create --name=caddy_data 

    # run docker-compose - assumes you have a docker-compose.yml file in the same directory
    docker-compose up -d 
else
    echo "No Caddyfile specified - stopping here"
fi
