# Docker setup with Caddy server

Contains the basic elements needed to set up [Caddy](https://caddyserver.com/) with Docker. 

Installation 
===
* Upload your website - it should be in a folder called `site`. If you want to change this and you're not using your own docker-compose file, remember to change this in the file.
* Build your `Caddyfile` (will need to change domain from localhost to something else) and optionally, your `docker-compose.yml` file(there is a default provided)
* Make `docker-setup.sh` executable with `chmod +x`
* Run `docker-setup.sh` (as root)
* That should 
  * Install Docker
  * Install Caddy 
  * Copy your `Caddyfile` and `site` folder into the container
  * Start the Caddy server.

And boom! Things should be running!.


# Notes
===
* Unfortunately Docker Compose needs to be installed from Github so it's a bit harder to make things automatic to pick up the latest version, you may need to manually edit `docker-setup.sh` in order to grab the latest in the future.

