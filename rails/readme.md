Rails 
====

Does a basic Ruby on Rails installation based on the tutorial [on the Docker Website](https://docs.docker.com/compose/rails/)

Installation 
=====
* `docker-compose build`
* Create a new Rails app with `docker-compose run web rails new <rails options` - this should create a new Rails app on the volume and copy the files to your working directory as well. 


Running
====
* `docker-compose up`

Notes
====
* Note that you may have to disable bootsnap if on Windows, for whatever reason it won't be able to read your Rails app folder. 
* Also, ensure that your Gemfile and Gemfile.lock have a Unix compatible line ending